import React from 'react'
import './App.css'
import { Draggable } from 'react-beautiful-dnd';
import {BiLike, BiMenuAltLeft, BiTime} from 'react-icons/bi'

function Task(props) {
    return (
        <Draggable draggableId={props.task.id} index={props.index}>
            {(provided, snapshot) => (
                <div className="new-column-style-div"
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                    isdragging={snapshot.isdragging}
                >
                    {
                        props.task.image ?
                        <img src={props.task.image} height="145px" />
                        : null
                    }
                    <p>
                    {props.task.header}
                    </p>
                    {
                        props.col !== 'column-3' && props.col !== 'column-2' ?
                        <small style={{color: 'grey'}}> <BiMenuAltLeft style={{position: 'relative', top: '2px', marginRight: '13px'}} /> <BiLike style={{position: 'relative', top: '2px'}} />  {props.task.like} </small> : null
                    }
                    {
                        props.col !== 'column-1' && props.col !== 'column-4' ?
                        <small style={{color: 'grey'}}> <BiTime /> {props.task.time} </small> : null
                    }
                    {
                        props.col !== 'column-1' ?
                        <img src={props.task.icon} height="35px" style={{borderRadius: '50%', position: 'relative', left: '120px', top: '5px'}}/> : null
                    }
                </div>
            )}
        </Draggable>
    )
}

export default Task