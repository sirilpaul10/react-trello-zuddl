import './App.css';
import React, { useState } from 'react';
import Navbar from './navbar/navbar.js';
import dataset from './mockData';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import TaskList from './List';

function App() {
  const [data, setData] = useState(dataset)

  const onDragEnd = result => {
    const { destination, source, draggableId } = result;

    if (!destination) { return }

    if (destination.droppableId === source.droppableId && destination.index === source.index) { return }
    const start = data.columns[source.droppableId];
    const finish = data.columns[destination.droppableId];

    if (start === finish) {
      const newTaskIds = Array.from(start.taskIds);
      newTaskIds.splice(source.index, 1);
      newTaskIds.splice(destination.index, 0, draggableId);
      const newColumn = {
        ...start,
        taskIds: newTaskIds
      }
      const newState = {
        ...data,
        columns: {
          ...data.columns,
          [newColumn.id]: newColumn
        }
      }
      setData(newState)
      return;
    }

    const startTaskIds = Array.from(start.taskIds);
    startTaskIds.splice(source.index, 1);
    const newStart = {
      ...start,
      taskIds: startTaskIds
    }

    const finishTaskIds = Array.from(finish.taskIds);
    finishTaskIds.splice(destination.index, 0, draggableId);
    const newFinish = {
      ...finish,
      taskIds: finishTaskIds
    }

    const newState = {
      ...data,
      columns: {
        ...data.columns,
        [newStart.id]: newStart,
        [newFinish.id]: newFinish
      }
    }

    setData(newState)
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId='all-columns' direction='horizontal' type='column'>
        {(provided) => (
          <>
          <div style={{backgroundImage: 'linear-gradient(90deg, rgb(64, 50, 148), rgb(76, 154, 255), rgb(64, 50, 148))'}}>
            <Navbar />
          </div>
          <div {...provided.droppableProps} ref={provided.innerRef} >
            {data.columnOrder.map((id, index) => {
              const column = data.columns[id]
              const tasks = column.taskIds.map(taskId => data.tasks[taskId])

              return <TaskList key={column.id} column={column} tasks={tasks} index={index} />
            })}
            {provided.placeholder}
          </div>
          </>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default App;
