const dataset = {
    tasks: {
        "task-1": { id: "task-1", header: "Financials and Growth Data", image: "https://images.unsplash.com/photo-1628348068343-c6a848d2b6dd", time: "Mar 31", like: "5", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-2": { id: "task-2", header: "2021 Goals and KPI's", image: "https://images.unsplash.com/photo-1563986768609-322da13575f3", time: "May 20", like: "4", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-3": { id: "task-3", header: "Brand Guide", image: "", time: "Aug 12", like: "5", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-4": { id: "task-4", header: "Employee Manual", image: "https://images.unsplash.com/photo-1581091877018-dac6a371d50f", time: "Jan 18", like: "1", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-5": { id: "task-5", header: "Build a better brunto: 7 layers to success", image: "", time: "Sep 10", like: "3", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-6": { id: "task-6", header: "Taco drone delivery service", image: "https://images.unsplash.com/photo-1477899447710-90571e12f4ae", time: "Oct 05", like: "2", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-7": { id: "task-7", header: "Eco friendly utensils and napkins", image: "", time: "Nov 10", like: "3", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" },
        "task-8": { id: "task-8", header: "Content for task-8", image: "", time: "Oct 10", like: "4", icon: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61" }
    },
    columns: {
        "column-1": { id: "column-1", title: "Resources", taskIds: ['task-1', 'task-5'] },
        "column-2": { id: "column-2", title: "To Do", taskIds: ['task-7', 'task-2'] },
        "column-3": { id: "column-3", title: "Doing", taskIds: ['task-6', "task-8"] },
        "column-4": { id: "column-4", title: "Done", taskIds: ['task-4', "task-3"] }
    },
    columnOrder: ["column-1", "column-2", "column-3", "column-4"]
}

export default dataset