import React from "react";
import "./navbar.css";
import {BiStar, BiDotsHorizontalRounded, BiLowVision} from 'react-icons/bi';
import {FaFortAwesome} from 'react-icons/fa';

export default class Navbar extends React.Component {
  render() {
    return (
     <ul>
      <li> <a> Taco's Tacos  <BiStar style={{marginLeft: '20px'}}/> </a> </li>
      <li> <a> <FaFortAwesome/> {'Taco & Co.'}  Free </a> </li>
      <li> <a> <BiLowVision/> Team Visible </a> </li>
      <li> <a>  </a> </li>
      <li> <a>  </a> </li>
      <li> <a>  </a> </li>
      <li> <a style={{textDecoration: 'underline', textAlign: 'end'}}> <BiDotsHorizontalRounded />  Show Menu </a>   </li>

     </ul>
    );
  }
}