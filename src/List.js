import React, { useState } from 'react';
import Task from './Task'
import { Droppable, Draggable } from 'react-beautiful-dnd';

function TaskList(props) {
    const [taskAddStatus, addTaskTextArea] = useState(false);
    const [task, setTask] = useState("");

    const handleClick = () => {
        addTaskTextArea(!taskAddStatus);
    }

    const addingTaskData = () => {
        console.log(props.column.id, task);
        handleClick();
    }

    return (
        <Draggable draggableId={props.column.id} index={props.index} isDragDisabled={true}>
            {(provided) => (
                <div className="column-list-style"
                    ref={provided.innerRef}
                    {...provided.draggableProps}>
                    <h3 className="head-tag-3-style" {...provided.dragHandleProps}>{props.column.title}</h3>
                    <Droppable droppableId={props.column.id} type='task'>
                        {(provided, snapshot) => (
                            <div className="task-list-style"
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                isdraggingover={snapshot.isdraggingover}
                            >
                                {props.tasks.map((task, index) => <Task key={task.id} task={task} index={index} col={props.column.id} />)}
                                {provided.placeholder}
                                    <div className="add-column-style-div" onClick={handleClick}>
                                        Add a card...
                                    </div>
                                    {
                                        taskAddStatus ?
                                        <div>
                                            <textarea className="add-task"
                                             value={task}
                                             onChange={e => setTask(e.target.value)}
                                            ></textarea>
                                            <button className="button-add-style" onClick={addingTaskData}>Add Task</button> 
                                            <button className="button-add-style" onClick={handleClick} style={{marginLeft: '10px'}}>Cancel</button>
                                        </div> : null
                                    }
                            </div>
                        )}
                    </Droppable>
                </div>

            )}
        </Draggable>
    )
}

export default TaskList
